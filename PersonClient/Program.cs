﻿using Grpc.Net.Client;
using PersonService;
using System;
using System.Threading.Tasks;

namespace PersonClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new PersonServices.PersonServicesClient(channel);
            Console.WriteLine("The name is: ");
            var name = Console.ReadLine();
            Console.WriteLine("The CNP is: ");
            var cnp = Console.ReadLine();
            var personAux = new Person() { Name = name.Trim().Length > 0 ? name : "anonym", Cnp = cnp.Trim().Length == 13 ? cnp : "undefined" };
            var response = await client.AddPersonAsync(
               new AddPersonRequest { Person = personAux });
            Console.WriteLine("Status for person added: " + response.Status);
        }
    }
}
