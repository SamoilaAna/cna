﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonService.Services
{
    public class GroupService : PersonServices.PersonServicesBase
    {
        private readonly ILogger<GroupService> _logger;
        public GroupService(ILogger<GroupService> logger)
        {
            _logger = logger;
        }

        public override Task<GetAllPersonsResponse> GetAllPersons(Empty request, ServerCallContext context)
        {
            _logger.Log(LogLevel.Information, "GetAllPersons Called");

            var response = new GetAllPersonsResponse();
            return Task.FromResult(response);
        }

        public override Task<AddPersonResponse> AddPerson(AddPersonRequest request, ServerCallContext context)
        {
            var person = request.Person;

            string auxCNP = person.Cnp;
            person.YearOfBirth = GetYearOfBirth(auxCNP).ToString();
            person.Gender = GetGender(auxCNP);

            _logger.Log(LogLevel.Information, "\nAdded person: " + person.Name + "\nBirth date: " + person.YearOfBirth + "\nGender: " + person.Gender);
            return Task.FromResult(new AddPersonResponse() { Status = AddPersonResponse.Types.Status.Succes });
        }

        public int GetYearOfBirth(string cnp)
        {
            int result;

            if(cnp[0]=='5'||cnp[0]=='6')
            {
                result = 20 * 100;
            }
            else
            {
                result = 19 * 100; 
            }
            if(cnp[1]!='0' && cnp[2]!='0')
            {
                int x = Int32.Parse(cnp[1].ToString());
                int y = Int32.Parse(cnp[2].ToString());
                result = result + x * 10 + y;
            }
            else
            {
                if(cnp[1]=='0' && cnp[2]!='0')
                {
                    int x = Int32.Parse(cnp[2].ToString());
                    result += x;
                }
                else
                {
                    if(cnp[1]!='0' && cnp[2]=='0')
                    {
                        int x = Int32.Parse(cnp[1].ToString());
                        result += x * 10;
                    }
                }
            }
            return result;
        }

        public string GetGender(string cnp)
        {
            string gender = null;
            if(cnp[0]=='5'||cnp[0]=='1')
            {
                gender = "male";
            }
            else
            {
                if(cnp[0]=='6'||cnp[0]=='2')
                {
                    gender = "female";
                }
            }

            return gender;
        }
    }
}
